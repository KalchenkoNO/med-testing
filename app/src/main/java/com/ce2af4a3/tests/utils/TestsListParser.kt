package com.ce2af4a3.tests.utils

import com.ce2af4a3.tests.domain.Test
import org.jsoup.Jsoup
import javax.inject.Inject

class TestsListParser @Inject constructor() : Parser<String, List<Test>> {
    override fun parse(input: String): List<Test> = Jsoup.parse(input).body()
        .getElementsByTag(LIST_TAG)
        .first()
        .getElementsByTag(LINK_TAG).map { element ->
            Test(
                name = element.text(),
                urn = element.attr(LINK_ATTR)
            )
        }

    companion object {
        private const val LIST_TAG = "ul"
        private const val LINK_TAG = "a"
        private const val LINK_ATTR = "href"
    }
}