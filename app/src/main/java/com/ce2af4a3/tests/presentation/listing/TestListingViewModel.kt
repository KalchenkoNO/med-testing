package com.ce2af4a3.tests.presentation.listing

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ce2af4a3.tests.domain.Test
import com.ce2af4a3.tests.domain.TestListingRepository
import kotlinx.coroutines.launch

class TestListingViewModel @ViewModelInject constructor(
    private val repo: TestListingRepository
) : ViewModel() {
    val liveTests: MutableLiveData<List<Test>> = MutableLiveData()
    val liveProgressIndicatorVisible: MutableLiveData<Boolean> = MutableLiveData()

    init {
        loadTests()
    }

    private fun loadTests() = viewModelScope.launch {
        liveProgressIndicatorVisible.value = true
        liveTests.value = repo.getTests()
        liveProgressIndicatorVisible.value = false
    }
}