package com.ce2af4a3.tests.presentation.question

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ce2af4a3.tests.R
import com.ce2af4a3.tests.databinding.ItemAnswerBinding
import com.ce2af4a3.tests.domain.Answer
import javax.inject.Inject

class AnswerViewHolder(
    private val viewBinding: ItemAnswerBinding
) : RecyclerView.ViewHolder(viewBinding.root) {
    fun bind(
        answer: Answer,
        listener: (Answer) -> Unit,
        choseMade: Boolean,
        chosen: Boolean
    ) = with(viewBinding) {
        title.text = answer.text
        bindCardView(answer.isCorrect, choseMade, chosen) { listener.invoke(answer) }
        bindIcon(answer.isCorrect, choseMade, chosen)
    }

    private inline fun bindCardView(
        isCorrect: Boolean,
        choseMade: Boolean,
        chosen: Boolean,
        crossinline listener: () -> Unit,
    ) = with(viewBinding.root) {
        if (!choseMade) setOnClickListener { listener() }
        strokeColor = when {
            !choseMade -> ResourcesCompat.getColor(resources, R.color.stroke, null)
            isCorrect -> ResourcesCompat.getColor(resources, R.color.good, null)
            chosen -> ResourcesCompat.getColor(resources, R.color.bad, null)
            else -> ResourcesCompat.getColor(resources, R.color.stroke, null)
        }
    }

    private fun bindIcon(
        isCorrect: Boolean,
        choseMade: Boolean,
        chosen: Boolean
    ) = with(viewBinding.icon) {
        setImageResource(
            if (isCorrect) {
                R.drawable.ic_check_circle_black_18dp
            } else {
                R.drawable.ic_cancel_24px
            }
        )
        isVisible = chosen || choseMade && isCorrect
    }
}

class AnswerAdapter @Inject constructor() : ListAdapter<Answer, AnswerViewHolder>(DIFF_CALLBACK) {
    private var listener: ((Answer) -> Unit)? = null
    private var choseMade: Boolean = false
    private var chosen: Answer? = null

    fun setCheckedListener(listener: (Answer) -> Unit) {
        this.listener = listener
    }

    private fun onItemPicked(answer: Answer) {
        choseMade = true
        chosen = answer
        notifyDataSetChanged()
        listener?.invoke(answer)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerViewHolder {
        return AnswerViewHolder(
            ItemAnswerBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: AnswerViewHolder, position: Int) {
        val chosen = choseMade && getItem(position) == chosen
        holder.bind(getItem(position), ::onItemPicked, choseMade, chosen)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Answer>() {
            override fun areItemsTheSame(oldItem: Answer, newItem: Answer): Boolean {
                return oldItem.text == newItem.text
            }

            override fun areContentsTheSame(oldItem: Answer, newItem: Answer): Boolean {
                return oldItem.text == newItem.text
            }
        }
    }
}