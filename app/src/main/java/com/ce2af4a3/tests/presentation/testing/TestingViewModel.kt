package com.ce2af4a3.tests.presentation.testing

import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ce2af4a3.tests.R
import com.ce2af4a3.tests.domain.Question
import com.ce2af4a3.tests.domain.TestData
import com.ce2af4a3.tests.domain.TestDataRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.util.*

class TestingViewModel @ViewModelInject constructor(
    @Assisted private val stateHandle: SavedStateHandle,
    private val repository: TestDataRepository,
    private val preferences: SharedPreferences
) : ViewModel() {
    val liveIsProgressVisible: MutableLiveData<Boolean> = MutableLiveData()
    val liveTitle: MutableLiveData<String> = MutableLiveData()
    val liveTestingDuration: MutableLiveData<String> = MutableLiveData()
    val liveCurrentQuestion: MutableLiveData<Question> = MutableLiveData()
    val liveIsButtonEnabled: MutableLiveData<Boolean> = MutableLiveData()
    val liveIsTextFieldEnabled: MutableLiveData<Boolean> = MutableLiveData<Boolean>().apply {
        value = true
    }
    val liveQuestionNumber: MutableLiveData<Pair<Int, Int>> = MutableLiveData()
    private var duration: Int? = null
    private var questionsTotal: Int = 0
        set(value) {
            if (value == field) return
            field = value
            onQuestionNumberChanged()
        }
    private var questionNumber: Int = 0
        set(value) {
            if (value == field) return
            field = value
            onQuestionNumberChanged()
        }
    private var questions: Queue<Question>? = null

    init {
        loadTestData()
    }

    private fun loadTestData() = viewModelScope.launch {
        liveIsProgressVisible.value = true
        runCatching {
            val testData = async { repository.getTestData(stateHandle[KEY_TEST_URN]!!) }
            liveTestingDuration.value = preferences.getInt(
                KEY_TESTING_DURATION,
                DEFAULT_TESTING_DURATION
            ).toString()
            onDataLoaded(testData.await())
        }.onFailure(::onError)
        liveIsProgressVisible.value = false
    }

    private fun onDataLoaded(testData: TestData) {
        liveTitle.value = testData.title
        questionsTotal = testData.questions.size
        questions = LinkedList(testData.questions.shuffled())
    }

    private fun onError(throwable: Throwable) {

    }

    private fun onQuestionNumberChanged() {
        liveQuestionNumber.value = questionNumber to questionsTotal
    }

    fun onClick(viewId: Int) {
        when (viewId) {
            R.id.start -> onStart()
        }
    }

    fun onTextChanged(viewId: Int, text: String) {
        duration = runCatching { text.toInt() }.getOrNull().also {
            liveIsButtonEnabled.value = it != null
        }
    }

    private fun onStart() = viewModelScope.launch {
        liveIsButtonEnabled.value = false
        liveIsTextFieldEnabled.value = false
        preferences.edit(true) {
            putInt(KEY_TESTING_DURATION, duration ?: DEFAULT_TESTING_DURATION)
        }
        questions?.poll()?.let { question ->
            questionNumber++
            liveCurrentQuestion.value = Question(
                text = question.text,
                answers = question.answers.shuffled()
            )
        }
    }

    companion object {
        const val DEFAULT_TESTING_DURATION = 60
        const val KEY_TESTING_DURATION = "testingDuration"
        private const val KEY_TEST_URN = "testUrn"
    }
}