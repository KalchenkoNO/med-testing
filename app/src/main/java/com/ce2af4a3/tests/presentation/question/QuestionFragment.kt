package com.ce2af4a3.tests.presentation.question

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.ce2af4a3.tests.R
import com.ce2af4a3.tests.databinding.FragmentQuestionBinding
import com.ce2af4a3.tests.presentation.testing.TestingViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class QuestionFragment : Fragment() {
    private val sharedViewModel by navGraphViewModels<TestingViewModel>(R.id.nav_testing) {
        defaultViewModelProviderFactory
    }
    private lateinit var viewBinding: FragmentQuestionBinding

    @Inject
    lateinit var answerAdapter: AnswerAdapter

    private var isFilled = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentQuestionBinding.inflate(
        inflater,
        container,
        false
    ).run {
        viewBinding = this
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeViewModel()
    }

    private fun initView() = with(viewBinding) {
        answers.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = answerAdapter
        }
    }

    private fun observeViewModel() = with(viewBinding) {
        sharedViewModel.liveCurrentQuestion.observe(viewLifecycleOwner) { questionData ->
            if (!isFilled) {
                question.text = questionData.text
                isFilled = true
            }

            answerAdapter.submitList(questionData.answers)
        }
        sharedViewModel.liveQuestionNumber.observe(viewLifecycleOwner) { (number, total) ->
            counter.text = getString(R.string.question_counter_format).format(number, total)
        }
    }
}