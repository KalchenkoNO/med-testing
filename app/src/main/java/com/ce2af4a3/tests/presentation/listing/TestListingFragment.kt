package com.ce2af4a3.tests.presentation.listing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ce2af4a3.tests.databinding.FragmentTestListingBinding
import com.ce2af4a3.tests.utils.isVisible
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TestListingFragment @Inject constructor() : Fragment() {
    private lateinit var viewBinding: FragmentTestListingBinding
    private val viewModel by viewModels<TestListingViewModel>()

    @Inject
    lateinit var testsAdapter: TestsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentTestListingBinding.inflate(inflater, container, false).let {
            viewBinding = it
            it.root
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        testsAdapter.setOnClickListener { test ->
            findNavController().navigate(
                TestListingFragmentDirections.actionStartTesting(test.urn)
            )
        }
        viewBinding.testsList.apply {
            adapter = testsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.liveTests.observe(viewLifecycleOwner) { availableTests ->
            testsAdapter.submitList(availableTests)
        }

        viewModel.liveProgressIndicatorVisible.observe(viewLifecycleOwner) { isVisible ->
            viewBinding.loadingIndicator.isVisible(isVisible)
        }
    }
}