package com.ce2af4a3.tests.presentation.starttesting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.navGraphViewModels
import com.ce2af4a3.tests.R
import com.ce2af4a3.tests.databinding.FragmentStartTestingBinding
import com.ce2af4a3.tests.presentation.testing.TestingViewModel
import com.ce2af4a3.tests.utils.isVisible
import com.ce2af4a3.tests.utils.lazyNavController
import com.ce2af4a3.tests.utils.supportActionBar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class StartTestingFragment : Fragment() {
    private val sharedVm by navGraphViewModels<TestingViewModel>(R.id.nav_testing) {
        defaultViewModelProviderFactory
    }

    private val navController by lazyNavController()
    private lateinit var viewBinding: FragmentStartTestingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentStartTestingBinding.inflate(
        inflater,
        container,
        false
    ).run {
        viewBinding = this
        root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        initView()
    }

    private fun initView() = with(viewBinding) {
        start.setOnClickListener { view ->
            sharedVm.onClick(view.id)
        }
        timer.doAfterTextChanged { text ->
            sharedVm.onTextChanged(timer.id, text.toString())
        }
    }

    private fun observeViewModel() = with(viewBinding) {
        sharedVm.liveIsProgressVisible.observe(viewLifecycleOwner) { isVisible ->
            loadingIndicator.isVisible(isVisible)
            content.isVisible = !isVisible
        }
        sharedVm.liveTitle.observe(viewLifecycleOwner) { supportActionBar()?.title = it }
        sharedVm.liveTestingDuration.observe(viewLifecycleOwner, timer::setText)
        sharedVm.liveIsButtonEnabled.observe(viewLifecycleOwner, start::setEnabled)
        sharedVm.liveIsTextFieldEnabled.observe(viewLifecycleOwner, timer::setEnabled)
        sharedVm.liveCurrentQuestion.observe(viewLifecycleOwner) {
            navController.navigate(
                StartTestingFragmentDirections.actionStartTestingFragmentToQuestionFragment()
            )
        }
    }
}